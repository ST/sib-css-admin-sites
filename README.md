# Introduction

The SIB CSS is based on the responsive *bootstrap* framework with some custom enhancements. 
It provides a good display on many devices (phones, tablets, laptops and desktop computers).

Documentation (guidlelines) on how the layout can be applied can be found in the following presentation:
https://drive.sib.swiss/index.php/s/NVQY8hhsLfUH0C8


# Guidelines

#### Page Header

Logo or name of website should appear on the _top left_ of the page
It is also link (URL) to the home page.
SIB logo should appear on the _top right_ of the page
If several logos are present, the SIB logo should always be _on the very right_.

#### Page Footer

‘SIB Swiss Institute of Bioinformatics’ (URL) should be present in the footer. 
If there are several institution URLs, SIB should always be _on the very left_.

#### Page Content

Currently, CSS classes are available for breadcrumbs, links, buttons, paginations, tables, and multi-columns.

# sib-css-admin-sites package

The sib-css-admin-sites package provides examples using a minimal version of the new SIB CSS. 

It is composed of 2 types of example pages:
- home pages
- other pages

with 2 types of displays (depending on users' needs):
- logo on the top left
- brand name on the top left

# Comments

#### Footer

We do not use any 'position' property for the CSS class 'footer'. Hence, the footer is not positioned in any special way; it is always positioned according to the normal flow of the page. For pages with few content, the footer can thus be in the middle of the page. In such case, you should either add the 'position' property to the 'footer' CSS class, with associated value 'absolute' (but beware of the display when resizing the page) or you should add a 'style' attribute to the div 'container' element with associated value 'min-height:1060px' (for example).



